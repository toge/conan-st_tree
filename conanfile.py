import os

from conans import ConanFile, tools

class StTreeConan(ConanFile):
    name = "st_tree"
    license = "Apache 2.0"
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-st_tree"
    homepage = "https://github.com/erikerlandson/st_tree/"
    description = "A C++ template tree container class for storing data in an arbitrary tree structure."
    topics = ("tree", "stl", "stl-container")
    no_copy_source = True
    # No settings/options are necessary, this is header only

    def source(self):
        '''retrieval of the source code here. Remember you can also put the code
        in the folder and use exports instead of retrieving it with this
        source() method
        '''
        tools.get(**self.conan_data["sources"][self.version])
        tools.get("https://github.com/erikerlandson/st_tree/archive/version_{}.zip".format(self.version))
        os.rename("st_tree-version_{}".format(self.version), "st_tree")

    def package(self):
        self.copy("*.h", "include", "st_tree/include")

